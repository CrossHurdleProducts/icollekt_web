<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

/**
 * App\Models\Slots
 *
 * @property int $id
 * @property string $name
 * @property string $type
 * @property string $description
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Slots newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Slots newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Slots query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Slots whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Slots whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Slots whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Slots whereDescription($value)
 * @mixin \Eloquent
 */

class Slots extends Model
{
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('alphabetical', function (Builder $builder) {
            $builder->orderBy('name', 'asc');
        });
    }
}