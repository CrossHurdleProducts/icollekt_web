<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

/**
 * App\Models\ProductCutting
 *
 * @property int $id
 * @property int $product_id
 * @property int $cutting_id
 * @property string $gross_weight
 * @property string $net_weight
 * @property int $cutting_charges
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductCutting newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductCutting newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductCutting query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductCutting whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductCutting whereProduct_id($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductCutting whereCutting_id($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductCutting whereGross_weight($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductCutting whereNet_weight($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductCutting whereCutting_charges($value)
 * @mixin \Eloquent
 */

class ProductCutting extends Model
{
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('alphabetical', function (Builder $builder) {
            $builder->orderBy('created_at', 'asc');
        });
    }
}