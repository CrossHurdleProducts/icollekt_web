<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

/**
 * App\Models\OrderSlots
 *
 * @property int $id
 * @property int $order_id
 * @property string $slot_name
 * @property string $slot_desc
 * @property string $slot_type
 * @property int $user_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrderSlots newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrderSlots newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrderSlots query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrderSlots whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrderSlots whereOrder_id($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrderSlots whereSlot_name($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrderSlots whereSlot_desc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrderSlots whereSlot_type($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrderSlots whereUser_id($value)
 * @mixin \Eloquent
 */

class OrderSlots extends Model
{
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('alphabetical', function (Builder $builder) {
            $builder->orderBy('id', 'asc');
        });
    }
}