<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

/**
 * App\Models\OrderProductCutting
 *
 * @property int $id
 * @property int $product_id
 * @property int $order_id
 * @property int $cutting_id
 * @property string $cutting_name
 * @property string $cutting_image
 * @property string $cutting_desc
 * @property string $gross_weight
 * @property string $net_weight
 * @property int $cutting_charges
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrderProductCutting newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrderProductCutting newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrderProductCutting query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrderProductCutting whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrderProductCutting whereProduct_id($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrderProductCutting whereOrder_id($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrderProductCutting whereCutting_id($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrderProductCutting whereCutting_name($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrderProductCutting whereCutting_image($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrderProductCutting whereCutting_desc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrderProductCutting whereGross_weight($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrderProductCutting whereNet_weight($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrderProductCutting whereCutting_charges($value)
 * @mixin \Eloquent
 */

class OrderProductCutting extends Model
{
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('alphabetical', function (Builder $builder) {
            $builder->orderBy('id', 'asc');
        });
    }
}