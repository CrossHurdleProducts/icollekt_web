<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

/**
 * App\Models\CartProductCuttings
 *
 * @property int $id
 * @property int $product_id
 * @property int $cart_id
 * @property int $user_id
 * @property int $cutting_id
 * @property string $cutting_name
 * @property string $cutting_image
 * @property string $cutting_desc
 * @property string $gross_weight
 * @property string $net_weight
 * @property int $cutting_charges
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CartProductCuttings newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CartProductCuttings newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CartProductCuttings query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CartProductCuttings whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CartProductCuttings whereProduct_id($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CartProductCuttings whereCart_id($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CartProductCuttings whereUser_id($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CartProductCuttings whereCutting_id($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CartProductCuttings whereCutting_name($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CartProductCuttings whereCutting_image($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CartProductCuttings whereCutting_desc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CartProductCuttings whereGross_weight($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CartProductCuttings whereNet_weight($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CartProductCuttings whereCutting_charges($value)
 * @mixin \Eloquent
 */

class CartProductCuttings extends Model
{
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('alphabetical', function (Builder $builder) {
            $builder->orderBy('id', 'asc');
        });
    }
}