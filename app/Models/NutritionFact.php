<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\NutritionFact
 *
 * @property int $id
 * @property int $product_id
 * @property string $calories
 * @property string $protein
 * @property string $fat
 * @property int $created_by
 * @property int $updated_by
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NutritionFact newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NutritionFact newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NutritionFact query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NutritionFact whereCreated_by($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NutritionFact whereUpdated_by($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NutritionFact whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NutritionFact whereProduct_id($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NutritionFact whereCalories($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NutritionFact whereProtein($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\NutritionFact whereFat($value)
 * @mixin \Eloquent
 */
class NutritionFact extends Model
{

}
