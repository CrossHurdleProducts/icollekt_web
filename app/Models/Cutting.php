<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

/**
 * App\Models\Cutting
 *
 * @property int $id
 * @property string $name
 * @property string|null $image
 * @property string|null $type
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cutting newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cutting newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cutting query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cutting whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cutting whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cutting whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cutting whereType($value)
 * @mixin \Eloquent
 */

class Cutting extends Model
{
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('alphabetical', function (Builder $builder) {
            $builder->orderBy('name', 'asc');
        });
    }
    public function order()
    {
        return $this->belongsTo(Order::class);
    }
}
