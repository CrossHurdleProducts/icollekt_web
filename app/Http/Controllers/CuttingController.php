<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Cutting;
use Illuminate\Support\Str;
//use Cache;

class CuttingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $sort_search =null;
        $cutting = Cutting::orderBy('id', 'desc');
        if ($request->has('search')){
            $sort_search = $request->search;
            $cutting = $cutting->where('name', 'like', '%'.$sort_search.'%');
        }
        $cutting = $cutting->paginate(15);
         
        return view('backend.cutting.index', compact('cutting', 'sort_search'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.cutting.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $cutting = new Cutting;
        $cutting->name = $request->name;
        $cutting->image = $request->image;
        $cutting->org_image = api_asset($request->image);
        $cutting->description = $request->description;
        $cutting->save();
       
        flash(translate('Cutting has been inserted successfully'))->success();
        return redirect()->route('cutting.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $lang = $request->lang;
        $cutting = Cutting::findOrFail($id);
        $cuttings = Cutting::where('id', 0)
            ->orderBy('name','asc')
            ->get();

        return view('backend.cutting.edit', compact('cutting', 'cuttings'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $cutting = Cutting::findOrFail($id);
        $cutting->name = $request->name;
        $cutting->image = $request->image;
        $cutting->org_image = api_asset($request->image);
        $cutting->description = $request->description;
        $cutting->save();

        flash(translate('Cutting has been updated successfully'))->success();
        return redirect()->route('cutting.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cutting = Cutting::destroy($id);

        flash(translate('Cutting has been deleted successfully'))->success();
        return redirect()->route('cutting.index');
    }

    /*
	public function updateFeatured(Request $request)
    {
        $cutting = Cutting::findOrFail($request->id);
        $cutting->featured = $request->status;
        $cutting->save();
        Cache::forget('featured_categories');
        return 1;
    }
	*/
}
