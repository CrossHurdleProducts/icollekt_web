<?php

namespace App\Http\Controllers\Api\V2;

use App\Models\Cart;
use App\Models\CartProductCuttings;
use App\Models\Cutting;
use App\Models\OrderProductCutting;
use App\Models\Product;
use App\Models\ProductCutting;
use App\Models\Slots;
use App\Shop;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CartController extends Controller
{
    public function summary($user_id)
    {
        $items = Cart::where('user_id', $user_id)->get();

        if ($items->isEmpty()) {
            return response()->json([
                'sub_total' => format_price(0.00),
                'tax' => format_price(0.00),
                'shipping_cost' => format_price(0.00),
                'discount' => format_price(0.00),
                'grand_total' => format_price(0.00),
                'grand_total_value' => 0.00,
                'coupon_code' => "",
                'coupon_applied' => false,
            ]);
        }

        $sum = 0.00;
        foreach ($items as $cartItem) {
            $item_sum = 0;
            $item_sum += ($cartItem->price + $cartItem->tax) * $cartItem->quantity;
            $item_sum += $cartItem->shipping_cost - $cartItem->discount;
            $sum +=  $item_sum  ;   //// 'grand_total' => $request->g
        }



        return response()->json([
            'sub_total' => format_price($items->sum('price')),
            'tax' => format_price($items->sum('tax')),
            'shipping_cost' => format_price($items->sum('shipping_cost')),
            'discount' => format_price($items->sum('discount')),
            'grand_total' => format_price($sum),
            'grand_total_value' => convert_price($sum),
            'coupon_code' => $items[0]->coupon_code,
            'coupon_applied' => $items[0]->coupon_applied == 1,
        ]);


    }

    public function getList($user_id)
    {
        $owner_ids = Cart::where('user_id', $user_id)->select('owner_id')->groupBy('owner_id')->pluck('owner_id')->toArray();
        $currency_symbol = currency_symbol();
        $shops = [];
        if (!empty($owner_ids)) {
            foreach ($owner_ids as $owner_id) {
                $shop = array();
                $shop_items_raw_data = Cart::where('user_id', $user_id)->where('owner_id', $owner_id)->get()->toArray();
                $shop_items_data = array();
                if (!empty($shop_items_raw_data)) {
                    
                    foreach ($shop_items_raw_data as $shop_items_raw_data_item) {
                        $product = Product::where('id', $shop_items_raw_data_item["product_id"])->first();

                        //product and cutting...
                        $product_cutting = ProductCutting::where('product_id',$product->id)->first();
                        if($product_cutting != null){
                            $product_cuttings = ($product_cutting->gross_weight != null && $product_cutting->net_weight != null && $product_cutting->cutting_charges != null) ? $product_cutting : null;
                            $cuttings = Cutting::where('id',$product_cutting->cutting_id)->get();
                            $cutting_data = ($cuttings != null)? $cuttings : '';
                        }else{
                            $cutting_data = [];
                            $product_cuttings = '';
                        }
                        
                        $shop_items_data_item["id"] = intval($shop_items_raw_data_item["id"]) ;
                        $shop_items_data_item["owner_id"] =intval($shop_items_raw_data_item["owner_id"]) ;
                        $shop_items_data_item["user_id"] =intval($shop_items_raw_data_item["user_id"]) ;
                        $shop_items_data_item["product_id"] =intval($shop_items_raw_data_item["product_id"]) ;
                        $shop_items_data_item["product_name"] = $product->name;
                        $shop_items_data_item["product_thumbnail_image"] = api_asset($product->thumbnail_img);
                        $shop_items_data_item["variation"] = $shop_items_raw_data_item["variation"];
                        $shop_items_data_item["price"] =(double) $shop_items_raw_data_item["price"];
                        $shop_items_data_item["currency_symbol"] = $currency_symbol;
                        $shop_items_data_item["tax"] =(double) $shop_items_raw_data_item["tax"];
                        $shop_items_data_item["shipping_cost"] =(double) $shop_items_raw_data_item["shipping_cost"];
                        $shop_items_data_item["quantity"] =intval($shop_items_raw_data_item["quantity"]) ;
                        $shop_items_data_item["cutting_data"] =$cutting_data;
                        $shop_items_data_item["product_cutting_details"] =[$product_cuttings];
                        $shop_items_data_item["lower_limit"] = intval($product->min_qty) ;
                        $shop_items_data_item["upper_limit"] = intval($product->stocks->where('variant', $shop_items_raw_data_item['variation'])->first()->qty) ;

                        $shop_items_data[] = $shop_items_data_item;
                    }
                }

                $shop_data = Shop::where('user_id', $owner_id)->first();
                if ($shop_data) {
                    $shop['name'] = $shop_data->name;
                    $shop['owner_id'] =(int) $owner_id;
                    $shop['cart_items'] = $shop_items_data;
                } else {
                    $shop['name'] = "Inhouse";
                    $shop['owner_id'] =(int) $owner_id;
                    $shop['cart_items'] = $shop_items_data;
                }
                $shops[] = $shop;
            }
        }
        //dd($shops);
        return response()->json(['cart' => $shops]);
    }


    public function add(Request $request)
    {
        $product = Product::findOrFail($request->id);  
        $variant = $request->variant;
        $tax = 0;

        if ($variant == '')
            $price = $product->unit_price;
        else {
            $product_stock = $product->stocks->where('variant', $variant)->first();
            $price = $product_stock->price;
        }

        //discount calculation based on flash deal and regular discount
        //calculation of taxes
        $discount_applicable = false;

        if ($product->discount_start_date == null) {
            $discount_applicable = true;
        }
        elseif (strtotime(date('d-m-Y H:i:s')) >= $product->discount_start_date &&
            strtotime(date('d-m-Y H:i:s')) <= $product->discount_end_date) {
            $discount_applicable = true;
        }

        if ($discount_applicable) {
            if($product->discount_type == 'percent'){
                $price -= ($price*$product->discount)/100;
            }
            elseif($product->discount_type == 'amount'){
                $price -= $product->discount;
            }
        }

        foreach ($product->taxes as $product_tax) {
            if ($product_tax->tax_type == 'percent') {
                $tax += ($price * $product_tax->tax) / 100;
            } elseif ($product_tax->tax_type == 'amount') {
                $tax += $product_tax->tax;
            }
        }

        if ($product->min_qty > $request->quantity) {
            return response()->json(['result' => false, 'message' => "Minimum {$product->min_qty} item(s) should be ordered"], 200);
        }
        $product_data = Product::where('id',$product->id)->first();
        $product_cutting = ProductCutting::where([['product_id',$product_data->id],['cutting_id',$request->cutting_id]])->first();
        if($product_cutting != null && $product_cutting->gross_weight != null && $product_cutting->net_weight != null && $product_cutting->cutting_charges != null){
            $product_cutting_id = $product_cutting->cutting_id;
            $gross_weight = $product_cutting->gross_weight;
            $net_weight = $product_cutting->net_weight;
            $cutting_charges = $product_cutting->cutting_charges;
        }else{
            $product_cutting_id ='';
            $gross_weight = '';
            $net_weight = '';
            $cutting_charges ='';
        }
        
        if($product_cutting_id != null){
			$cutting = Cutting::where('id',$product_cutting->cutting_id)->first();
        }else{
			$cut_name = '';
			$cut_image = '';
			$cut_desc = '';
        }
        
        $stock = $product->stocks->where('variant', $variant)->first()->qty;
        
        $variant_string = $variant != null && $variant != "" ? "for ($variant)" : "";
        if ($stock < $request->quantity) {
            if ($stock == 0) {
                return response()->json(['result' => false, 'message' => "Stock out"], 200);
            } else {
                return response()->json(['result' => false, 'message' => "Only {$stock} item(s) are available {$variant_string}"], 200);
            }
        }
        
        $data = Cart::updateOrCreate([
            'user_id' => $request->user_id,
            'owner_id' => $product->user_id,
            'product_id' => $product->id,
            'variation' => $variant
        ], [
            'price' => $price,
            'tax' => $tax,
            'shipping_cost' => 0,
            'quantity' => DB::raw("quantity + $request->quantity")
        ]);
        
        $cart_cutting = DB::table('carts')
        ->leftJoin('cart_product_cuttings','carts.id','=','cart_product_cuttings.cart_id')
        ->where('cart_product_cuttings.user_id', $request->user_id)
        ->where('cart_product_cuttings.product_id',$product->id)
        ->first();
        // print_r('----------------');exit;
        if($cart_cutting != null){
            $cart_product_cutting_data = CartProductCuttings::findOrFail($cart_cutting->id);
            $cart_product_cutting_data->cutting_id = $product_cutting_id;
            $cart_product_cutting_data->product_id = $product->id;
            $cart_product_cutting_data->cart_id = $data->id;
            $cart_product_cutting_data->user_id = $request->user_id;
            $cart_product_cutting_data->cutting_name = ($product_cutting_id != null) ? $cutting->name :  $cut_name;
            $cart_product_cutting_data->cutting_image = ($product_cutting_id != null)? $cutting->image : $cut_image;
            $cart_product_cutting_data->cutting_desc = ($product_cutting_id != null)? $cutting->description : $cut_desc;
            $cart_product_cutting_data->gross_weight = $gross_weight;
            $cart_product_cutting_data->net_weight = $net_weight;
            $cart_product_cutting_data->cutting_charges = $cutting_charges;
            $cart_product_cutting_data->updated_by = $request->user_id;
            $cart_product_cutting_data->save();
        }
        
        if($cart_cutting == null && $product_cutting_id != null ){ 
			$cart_product_cutting_data = new CartProductCuttings;
			$cart_product_cutting_data->cutting_id = $product_cutting_id;
			$cart_product_cutting_data->product_id = $product->id;
			$cart_product_cutting_data->cart_id = $data->id;
			$cart_product_cutting_data->user_id = $request->user_id;
			$cart_product_cutting_data->cutting_name = ($product_cutting_id != null) ? $cutting->name : $cut_name;
			$cart_product_cutting_data->cutting_image = ($product_cutting_id != null)? $cutting->image : $cut_image;
			$cart_product_cutting_data->cutting_desc = ($product_cutting_id != null)? $cutting->description : $cut_desc;
			$cart_product_cutting_data->gross_weight = $gross_weight;
			$cart_product_cutting_data->net_weight = $net_weight;
			$cart_product_cutting_data->cutting_charges = $cutting_charges;
			$cart_product_cutting_data->save();
        }

        // if(\App\Utility\NagadUtility::create_balance_reference($request->cost_matrix) == false){
        //     return response()->json(['result' => false, 'message' => 'Cost matrix error' ]);
        // }

        return response()->json([
            'result' => true,
            'message' => 'Product added to cart successfully'
			// //'Cart' => [$data,['product_data'=>$product_data,'Cutting_data' => [$cart_product_cutting_data]]],
        ]);
    }

    public function changeQuantity(Request $request)
    {
        $cart = Cart::find($request->id);
        if ($cart != null) {

            if ($cart->product->stocks->where('variant', $cart->variation)->first()->qty >= $request->quantity) {
                $cart->update([
                    'quantity' => $request->quantity
                ]);

                return response()->json(['result' => true, 'message' => 'Cart updated'], 200);
            } else {
                return response()->json(['result' => false, 'message' => 'Maximum available quantity reached'], 200);
            }
        }

        return response()->json(['result' => false, 'message' => 'Something went wrong'], 200);
    }

    public function process(Request $request)
    {
        $cart_ids = explode(",", $request->cart_ids);
        $cart_quantities = explode(",", $request->cart_quantities);

        if (!empty($cart_ids)) {
            $i = 0;
            foreach ($cart_ids as $cart_id) {
                $cart_item = Cart::where('id', $cart_id)->first();
                $product = Product::where('id', $cart_item->product_id)->first();

                if ($product->min_qty > $cart_quantities[$i]) {
                    return response()->json(['result' => false, 'message' => "Minimum {$product->min_qty} item(s) should be ordered for {$product->name}"], 200);
                }

                $stock = $cart_item->product->stocks->where('variant', $cart_item->variation)->first()->qty;
                $variant_string = $cart_item->variation != null && $cart_item->variation != "" ? " ($cart_item->variation)" : "";
                if ($stock >= $cart_quantities[$i]) {
                    $cart_item->update([
                        'quantity' => $cart_quantities[$i]
                    ]);

                } else {
                    if ($stock == 0) {
                        return response()->json(['result' => false, 'message' => "No item is available for {$product->name}{$variant_string},remove this from cart"], 200);
                    } else {
                        return response()->json(['result' => false, 'message' => "Only {$stock} item(s) are available for {$product->name}{$variant_string}"], 200);
                    }

                }

                $i++;
            }

            return response()->json(['result' => true, 'message' => 'Cart updated'], 200);

        } else {
            return response()->json(['result' => false, 'message' => 'Cart is empty'], 200);
        }


    }

    public function destroy($id)
    {
        Cart::destroy($id);
        return response()->json(['result' => true, 'message' => 'Product is successfully removed from your cart'], 200);
    }
}
