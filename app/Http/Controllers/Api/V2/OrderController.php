<?php

namespace App\Http\Controllers\Api\V2;

use App\Address;
use Illuminate\Http\Request;
use App\Models\Order;
use App\Models\Cart;
use App\Models\Product;
use App\Models\OrderDetail;
use App\Models\Coupon;
use App\Models\CouponUsage;
use App\Models\BusinessSetting;
use App\User;
use DB;
use \App\Utility\NotificationUtility;
use App\CombinedOrder;
use App\Http\Controllers\AffiliateController;
use App\Models\CartProductCuttings;
use App\Models\Category;
use App\Models\OrderProductCutting;
use App\Models\OrderSlots;
use App\Models\Slots;
use Throwable;

class OrderController extends Controller
{
    public function store(Request $request, $set_paid = false)
    {
        $cartItems = Cart::where('user_id', $request->user_id)->get();

        if ($cartItems->isEmpty()) {
            return response()->json([
                'combined_order_id' => 0,
                'result' => false,
                'message' => 'Cart is Empty'
            ]);
        }

        $user = User::find($request->user_id);


        $address = Address::where('id', $cartItems->first()->address_id)->first();
        $shippingAddress = [];
        if ($address != null) {
            $shippingAddress['name']        = $user->name;
            $shippingAddress['email']       = $user->email;
            $shippingAddress['address']     = $address->address;
            $shippingAddress['country']     = $address->country;
            $shippingAddress['city']        = $address->city;
            $shippingAddress['postal_code'] = $address->postal_code;
            $shippingAddress['phone']       = $address->phone;
            if ($address->latitude || $address->longitude) {
                $shippingAddress['lat_lang'] = $address->latitude . ',' . $address->longitude;
            }
        }

        $combined_order = new CombinedOrder;
        $combined_order->user_id = $user->id;
        $combined_order->shipping_address = json_encode($shippingAddress);
        $combined_order->save();

        $seller_products = array();
        foreach ($cartItems as $cartItem) {
            $product_ids = array();
            $product = Product::find($cartItem['product_id']);
            if (isset($seller_products[$product->user_id])) {
                $product_ids = $seller_products[$product->user_id];
            }
            array_push($product_ids, $cartItem);
            $seller_products[$product->user_id] = $product_ids;
        }

        foreach ($seller_products as $seller_product) {
            $order = new Order;
            $order->combined_order_id = $combined_order->id;
            $order->user_id = $user->id;
            $order->shipping_address = json_encode($shippingAddress);

            $order->payment_type = $request->payment_option;
            $order->delivery_viewed = '0';
            $order->payment_status_viewed = '0';
            $order->code = date('Ymd-His') . rand(10, 99);
            $order->date = strtotime('now');
            $order->save();

            $subtotal = 0;
            $tax = 0;
            $shipping = 0;
            $coupon_discount = 0;

            //Order Details Storing
            foreach ($seller_product as $cartItem) {
                $product = Product::find($cartItem['product_id']);

                $subtotal += $cartItem['price'] * $cartItem['quantity'];
                $tax += $cartItem['tax'] * $cartItem['quantity'];
                $coupon_discount += $cartItem['discount'];

                $product_variation = $cartItem['variation'];

                $product_stock = $product->stocks->where('variant', $product_variation)->first();
                if ($product->digital != 1 && $cartItem['quantity'] > $product_stock->qty) {
                    $order->delete();
                    $combined_order->delete();
                    return response()->json([
                        'combined_order_id' => 0,
                        'result' => false,
                        'message' => 'The requested quantity is not available for ' . $product->name
                    ]);
                } elseif ($product->digital != 1) {
                    $product_stock->qty -= $cartItem['quantity'];
                    $product_stock->save();
                }

                $order_detail = new OrderDetail;
                $order_detail->order_id = $order->id;
                $order_detail->seller_id = $product->user_id;
                $order_detail->product_id = $product->id;
                $order_detail->variation = $product_variation;
                $order_detail->price = $cartItem['price'] * $cartItem['quantity'];
                $order_detail->tax = $cartItem['tax'] * $cartItem['quantity'];
                $order_detail->shipping_type = $cartItem['shipping_type'];
                $order_detail->product_referral_code = $cartItem['product_referral_code'];
                $order_detail->shipping_cost = $cartItem['shipping_cost'];

                $shipping += $order_detail->shipping_cost;

                if ($cartItem['shipping_type'] == 'pickup_point') {
                    $order_detail->pickup_point_id = $cartItem['pickup_point'];
                }
                //End of storing shipping cost

                $order_detail->quantity = $cartItem['quantity'];
                $order_detail->save();

                $product->num_of_sale = $product->num_of_sale + $cartItem['quantity'];
                $product->save();

                $order->seller_id = $product->user_id;

               
                if (addon_is_activated('affiliate_system')) {
                    if ($order_detail->product_referral_code) {
                        $referred_by_user = User::where('referral_code', $order_detail->product_referral_code)->first();

                        $affiliateController = new AffiliateController;
                        $affiliateController->processAffiliateStats($referred_by_user->id, 0, $order_detail->quantity, 0, 0);
                    }
                } 

            }

            $order->grand_total = $subtotal + $tax + $shipping;

            if ($seller_product[0]->coupon_code != null) {
                // if (Session::has('club_point')) {
                //     $order->club_point = Session::get('club_point');
                // }
                $order->coupon_discount = $coupon_discount;
                $order->grand_total -= $coupon_discount;

                $coupon_usage = new CouponUsage;
                $coupon_usage->user_id = $user->id;
                $coupon_usage->coupon_id = Coupon::where('code', $seller_product[0]->coupon_code)->first()->id;
                $coupon_usage->save();
            }

            $combined_order->grand_total += $order->grand_total;

            if (strpos($request->payment_type, "manual_payment_") !== false) { // if payment type like  manual_payment_1 or  manual_payment_25 etc)

                $order->manual_payment = 1;
                $order->save();

            }

            $order->save();
        }
        $order_cutting_data = CartProductCuttings::where('user_id',$request->user_id)->get();
        
        for($i=0; count($order_cutting_data) > $i; $i++){
            $orders_cuttings = new OrderProductCutting;
            $orders_cuttings->product_id = $order_cutting_data[$i]->product_id;
            $orders_cuttings->order_id = $order->id;
            $orders_cuttings->cutting_id = ($order_cutting_data[$i]->cutting_id != null)?$order_cutting_data[$i]->cutting_id:'';
            $orders_cuttings->cutting_name = ($order_cutting_data[$i]->cutting_name != null)?$order_cutting_data[$i]->cutting_name:'';
            $orders_cuttings->cutting_image = ($order_cutting_data[$i]->cutting_image != null)?$order_cutting_data[$i]->cutting_image:'';
            $orders_cuttings->cutting_desc = ($order_cutting_data[$i]->cutting_desc != null)?$order_cutting_data[$i]->cutting_desc:'';
            $orders_cuttings->gross_weight = ($order_cutting_data[$i]->gross_weight != null)?$order_cutting_data[$i]->gross_weight:'';
            $orders_cuttings->net_weight = ($order_cutting_data[$i]->net_weight != null)?$order_cutting_data[$i]->net_weight:'';
            $orders_cuttings->cutting_charges = ($order_cutting_data[$i]->cutting_charges != null)?$order_cutting_data[$i]->cutting_charges : '';
            $orders_cuttings->created_by = $order_cutting_data[$i]->user_id;
            $orders_cuttings->save();
        } 

        // $slot = OrderSlots::all();
        // print_r($slot);exit;
         $slot_data = Slots::where('id',$request->slot_id)->first();

         $order_slots = new OrderSlots;
         $order_slots->order_id = $order->id;
         $order_slots->slot_id = $slot_data->id;
         $order_slots->slot_name = $slot_data->name;
         $order_slots->slot_desc = $slot_data->description;
         $order_slots->slot_type = $slot_data->type;
         $order_slots->start_time = $slot_data->start_time;
         $order_slots->end_time = $slot_data->end_time;
         $order_slots->created_by = $request->user_id;
         //print_r($order_slots);exit;
         $order_slots->save();

        $combined_order->save();

        CartProductCuttings::where('user_id', $request->user_id)->delete();
        Cart::where('user_id', $request->user_id)->where('owner_id', $request->owner_id)->delete();

        if (
            $request->payment_type == 'cash_on_delivery'
            || $request->payment_type == 'wallet'
            || strpos($request->payment_type, "manual_payment_") !== false // if payment type like  manual_payment_1 or  manual_payment_25 etc
        ) {
            NotificationUtility::sendOrderPlacedNotification($order);
        }


        return response()->json([
            //'Order details' => $order_cutting,
            'combined_order_id' => $combined_order->id,
            'result' => true,
            'message' => translate('Your order has been placed successfully')
        ]);
    }

    ///////////////////////////////////////////////////////////////////////  New  /////////////////////////////////////////////////////////////////////////
    public function all_orders($id)
    {
        $order = Order::findOrFail($id);
        $order_shipping_address = json_decode($order->shipping_address);
        $delivery_boys = User::where('city', $order_shipping_address->city)
            ->where('user_type', 'delivery_boy')
            ->get();
        //dd($delivery_boys);
        $order_details = DB::table('order_details')
        ->leftJoin('orders','order_details.order_id','=','orders.id')
        ->where('orders.id',$order->id)
        ->first();
        //dd($order_details);

         $users = DB::table('orders')
        ->leftJoin('users','orders.user_id','=','users.id')
        ->get(['user_type','name','email','phone','address','city','postal_code','shipping_address'])
        ->first();
        //dd($users);

        // $product_cutting = DB::table('order_product_cuttings')
        // ->leftJoin('order_details','order_product_cuttings.order_id','=','order_details.order_id')
        // ->where('order_product_cuttings.order_id',$order->id)
        // ->groupBy('order_product_cuttings.cutting_id')
        // ->get();
        //dd($product_cutting);

        $slot_data = DB::table('order_slots')
        ->leftJoin('order_details','order_slots.order_id','=','order_details.order_id')
        ->get(['slot_name','slot_desc','slot_type','start_time','end_time'])
        ->first();
        //dd($slot_data);
        
        $product_data = DB::table('order_details')
        ->leftJoin('products','order_details.product_id','=','products.id')
        ->join('order_product_cuttings','order_details.order_id','=','order_product_cuttings.order_id')
        ->join('categories','products.category_id','=','categories.id')
        ->where('order_details.order_id',$order->id)
        ->groupBy('order_product_cuttings.id')
        ->get();

        //$products_datas = json_encode($product_data);
        //dd($product_data);
 
        $categories = DB::table('categories')
        ->leftJoin('products','categories.id','=','products.category_id')
        ->get();
        //dd($categories);

        return response()->json([
         'Order_details' => [$order_details,['order_slot'=>[ $slot_data]]],
         'customer' => [$users],
         'product_data' => $product_data,
        ]);
    }
}
