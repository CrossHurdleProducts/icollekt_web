<?php

namespace App\Http\Controllers\Api\V2;

use App\Http\Resources\V2\BannerCollection;
use App\Models\Banner;

class BannerController extends Controller
{

    public function index()
    {
        return new BannerCollection(json_decode(get_setting('home_banner1_images'), true));
    }
	
	public function get_all()
    {
		$data['data'] = Banner::where('published', 1)->get();	
        return $data;
    }	 
}
