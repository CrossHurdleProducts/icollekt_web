<?php

namespace App\Http\Controllers\Api\V2;

use Illuminate\Http\Request;
use App\Models\Slots;
use Illuminate\Support\Facades\DB;

class SlotController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
		$slot = Slots::all();

		return response()->json([
			'data' => $slot
		]);
    }
	
	/**
     * Display a listing of the resource by Slot Type.
     *
     * @return \Illuminate\Http\Response
     */
    public function slots_by_type(Request $request)
    { 	
		$slots = DB::table('slots')->get();//print_r($slots);die;
		
		$slot_data['early_morning'] = [];
		$slot_data['morning'] 		= [];
		$slot_data['afternoon'] 	= [];
		$slot_data['evening'] 		= []; 
		
		foreach ($slots as $slot)
		{
			if($slot->type == "Early Morning"){
				array_push($slot_data['early_morning'],$slot);
			}
			else if($slot->type == "Morning"){
				array_push($slot_data['morning'],$slot);
			}
			else if($slot->type == "Afternoon"){
				array_push($slot_data['afternoon'],$slot);
			}
			else if($slot->type == "Evening"){
				array_push($slot_data['evening'],$slot);
			}
			 
		}

		return response()->json([
			'data' => $slot_data
		]);
    }
}