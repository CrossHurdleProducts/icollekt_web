<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Customer;
use App\User;
use App\Order;
use App\Models\Faq;
use App\Country;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $sort_search = null;
        $pageLimit = 10;
        $customersQuery = Customer::with('user')->join('users', 'users.id','=','customers.user_id');
        if ($request->has('search')){
            $sort_search = $request->search;
            $customersQuery->where(function($user) use ($sort_search){
                $user->where('users.name', 'like', '%'.$sort_search.'%');
                $user->orWhere('users.email', 'like', '%'.$sort_search.'%');
            });
        }
        $customersQuery->select('customers.*');
        $customers = $customersQuery->paginate($pageLimit);
        return view('backend.customer.customers.index', compact('customers', 'sort_search'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'          => 'required',
            'email'         => 'required|unique:users|email',
            'phone'         => 'required|unique:users',
        ]);
        
        $response['status'] = 'Error';
        
        $user = User::create($request->all());
        
        $customer = new Customer;
        
        $customer->user_id = $user->id;
        $customer->save();
        
        if (isset($user->id)) {
            $html = '';
            $html .= '<option value="">
                        '. translate("Walk In Customer") .'
                    </option>';
            foreach(Customer::all() as $key => $customer){
                if ($customer->user) {
                    $html .= '<option value="'.$customer->user->id.'" data-contact="'.$customer->user->email.'">
                                '.$customer->user->name.'
                            </option>';
                }
            }
            
            $response['status'] = 'Success';
            $response['html'] = $html;
        }
        
        echo json_encode($response);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $customer = User::find($id);
        return view('backend.customer.customers.edit', compact('customer'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name'          => 'required|max:100',
            'email'         => 'required|max:100|unique:users,email,'.$id,
        ]);
    
        $UpdateDetails = User::where('id', '=',  $id)->first();
        $UpdateDetails->name = $request->name;
        $UpdateDetails->email = $request->email;
        $UpdateDetails->address = $request->address;
        $UpdateDetails->date_of_birth = $request->date_of_birth;
        $UpdateDetails->phone = $request->phone;
        $UpdateDetails->city = $request->city;
        $UpdateDetails->country = $request->country;
        $UpdateDetails->postal_code = $request->postal_code;
        $UpdateDetails->save();
        flash(translate('Customer information has been updated successfully'))->success();
        return redirect()->route('customers.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Order::where('user_id', Customer::findOrFail($id)->user->id)->delete();
        User::destroy(Customer::findOrFail($id)->user->id);
        if(Customer::destroy($id)){
            flash(translate('Customer has been deleted successfully'))->success();
            return redirect()->route('customers.index');
        }

        flash(translate('Something went wrong'))->error();
        return back();
    }
    
    public function bulk_customer_delete(Request $request) {
        if($request->id) {
            foreach ($request->id as $customer_id) {
                $this->destroy($customer_id);
            }
        }
        
        return 1;
    }

    public function login($id)
    {
        $customer = Customer::findOrFail(decrypt($id));

        $user  = $customer->user;

        auth()->login($user, true);

        return redirect()->route('dashboard');
    }

    public function ban($id) {
        $customer = Customer::findOrFail($id);

        if($customer->user->banned == 1) {
            $customer->user->banned = 0;
            flash(translate('Customer UnBanned Successfully'))->success();
        } else {
            $customer->user->banned = 1;
            flash(translate('Customer Banned Successfully'))->success();
        }

        $customer->user->save();

        return back();
    }
    
    public function faq(Request $request) {
        $sort_search = null;
        $faqs = Faq::orderBy('created_at', 'desc');

        if ($request->search != null){
            $faqs = $faqs->where('question', 'like', '%'.$request->search.'%')->orWhere('answer', 'like', '%'.$request->search.'%');
            $sort_search = $request->search;
        }
        $faqs = $faqs->paginate(4);
        return view('backend.customer.faq.index', compact('faqs', 'sort_search'));
    }
    
    public function create_faq(Request $request)
    {
        return view('backend.customer.faq.create');
    }
    
    public function store_faq(Request $request) {
        $validated = $request->validate([
            'question' => 'required|max:255',
            'answer' => 'required|max:255',
        ]);
        $data = new Faq;
        $data->question = $request->question;
        $data->answer = $request->answer;
        $data->save();
        flash(translate('Faq has been inserted successfully'))->success();
        return redirect()->route('faq');
    }
    
    public function edit_faq(Request $request, $id)
    {
        $faq = Faq::findOrFail($id);
        return view('backend.customer.faq.edit', compact('faq'));
    }

    public function update_faq(Request $request, $id)
    {
        $data = Faq::findOrFail($id);
        $request->validate([
            'question' => 'required|max:255',
            'answer' => 'required|max:255',
        ]);
        $data->question = $request->question;
        $data->answer = $request->answer;
        $data->save();

        flash(translate('Faq has been updated successfully'))->success();
        return redirect()->route('faq');
    }
    
    public function destroy_faq($id)
    {
        Faq::destroy($id);
        flash(translate('Faq has been deleted successfully'))->success();
        return redirect()->route('faq');

    }
}
