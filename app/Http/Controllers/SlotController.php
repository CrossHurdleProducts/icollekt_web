<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Slots;
use Cache;

class SlotController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
       $sort_search =null;
        $slots = Slots::orderBy('id', 'desc');
        if ($request->has('search')){
            $sort_search = $request->search;
            $slots = $slots->where('name', 'like', '%'.$sort_search.'%');
        }
        $slots = $slots->paginate(15);
        return view('backend.slots.index', compact('slots','sort_search'));
    }
      /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.slots.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $slat = new Slots;
        $slat->name = $request->name;
        $slat->type = $request->type;
        $slat->description = $request->description;
        $slat->start_time = $request->start_time;
        $slat->end_time = $request->end_time;
        $slat->save();
       
        flash(translate('Slot has been inserted successfully'))->success();
        return redirect()->route('slots.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $lang = $request->lang;
        $slot = Slots::findOrFail($id);
        $slots = Slots::where('id', 0)
            ->orderBy('name','asc')
            ->get();

        return view('backend.slots.edit', compact('slot', 'slots'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $slat = Slots::findOrFail($id);
        $slat->name = $request->name;
        $slat->type = $request->type;
        $slat->description = $request->description;
        $slat->start_time = $request->start_time;
        $slat->end_time = $request->end_time;
        $slat->save();

        flash(translate('Slot has been updated successfully'))->success();
        return redirect()->route('slots.index');
    }

  /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $slat = Slots::destroy($id);

        flash(translate('Slot has been deleted successfully'))->success();
        return redirect()->route('slots.index');
    }

    public function updateFeatured(Request $request)
    {
        $slat = Slots::findOrFail($request->id);
        $slat->featured = $request->status;
        $slat->save();
        Cache::forget('featured_categories');
        return 1;
    }
}
