<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App;

class Collection extends Model
{
    protected $fillable = ['title'];

    public $timestamps = false;

}
