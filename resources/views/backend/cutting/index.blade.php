@extends('backend.layouts.app')

@section('content')
<div class="aiz-titlebar text-left mt-2 mb-3">
    <div class="row align-items-center">
        <div class="col-md-6">
            <h1 class="h3">All Cuttings</h1>
        </div>
    </div>
</div>
<div class="card">
    <div class="card-header d-block d-md-flex">
        <h5 class="mb-0 h6">Cuttings</h5>
        <form class="" id="sort_cutting" action="" method="GET">
            <div class="box-inline pad-rgt pull-left">
                <div class="" style="min-width: 200px;">
                    <input type="text" class="form-control" id="search" name="search"@isset($sort_search) value="{{ $sort_search }}" @endisset placeholder="Type name & Enter">
                </div>
            </div>
        </form>
    </div>
    <div class="card-body">
        <table class="table aiz-table mb-0">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Image</th>
                    <th>Name</th>
                    <th>Description</th>
                    <th>Option</th>
                </tr>
            </thead>
            <tbody>
                @foreach($cutting as $key => $cut)
                    <tr>
                        <td>{{ $key+1 }}</td>
			            <td> 
							@if($cut->image != null)
                                <img src="{{ uploaded_asset($cut->image) }}" alt="Cutting image" class="h-50px">
                            @else
                                —
                            @endif
						</td> 
                        <td>{{ $cut->name }}</td>
			            <td>{{ $cut->description }}</td>
                        <td class="text-right">
                            <a class="btn btn-soft-primary btn-icon btn-circle btn-sm" href="{{route('cutting.edit', ['id' => $cut->id ] )}}" title="Edit">
                                <i class="las la-edit"></i>
                            </a>
                            <a href="#" class="btn btn-soft-danger btn-icon btn-circle btn-sm confirm-delete" data-href="{{route('cutting.destroy', $cut->id)}}" title="Delete">
                                <i class="las la-trash"></i>
                            </a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection


@section('modal')
    @include('modals.delete_modal')
@endsection

