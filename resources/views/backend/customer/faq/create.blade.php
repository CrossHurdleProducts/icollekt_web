@extends('backend.layouts.app')
@section('content')
<div class="aiz-titlebar text-left mt-2 mb-3">
    <h5 class="mb-0 h6">{{translate('New FAQ')}}</h5>
</div>
<div class="col-lg-8">
    <div class="card">
        <div class="card-body p-0">
            @if ($errors->any())
            <div class="alert alert-danger mr-4 ml-4 mt-4">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            <form class="p-4" action="{{ route('faq.store') }}" method="POST">
                @csrf
                <div class="form-group row">
                    <div class="col-sm-12">
                        <label for="name">{{ translate('Question') }}</label>
                        <textarea placeholder="{{ translate('Question') }}" id="question" name="question" class="form-control" value="{{ old('question') }}" required></textarea>
                    </div>
                    <div class="col-sm-12">
                        <label for="name">{{ translate('Answer') }}</label>
                        <textarea placeholder="{{ translate('Answer') }}" id="answer" name="answer" class="form-control" value="{{ old('answer') }}" required></textarea>
                    </div>
                </div>
                <div class="form-group mb-0 text-right">
                    <button type="submit" class="btn btn-primary">{{translate('Save')}}</button>
                    <a href="{{ route('faq')  }}" class="btn btn-danger">{{translate('Cancel')}}</a>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
