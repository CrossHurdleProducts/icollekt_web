@extends('backend.layouts.app')
@section('content')
<div class="aiz-titlebar text-left mt-2 mb-3">
    <div class="row align-items-center">
        <div class="col-auto">
            <h1 class="h3">{{translate('All Faqs')}}</h1>
        </div>
        <div class="col text-right">
            <a href="{{ route('faq.create') }}" class="btn btn-circle btn-info">
                <span>{{translate('Add New Faq')}}</span>
            </a>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <form class="" id="sort_colors" action="" method="GET">
                <div class="card-header">
                    <h5 class="mb-0 h6">{{ translate('Faqs') }}</h5>
                    <div class="col-md-5">
                        <div class="form-group mb-0">
                            <input type="text" class="form-control form-control-sm" id="search" name="search"
                            @isset($sort_search) value="{{ $sort_search }}" @endisset
                            placeholder="{{ translate('Type color name & Enter') }}">
                        </div>
                    </div>
                </div>
            </form>
            <div class="card-body">
                <table class="table aiz-table mb-0">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>{{ translate('Question') }}</th>
                            <th>{{ translate('Answer') }}</th>
                            <th class="text-right">{{ translate('Options') }}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($faqs as $key => $faq)
                        <tr>
                            <td>{{ ($key+1) + ($faqs->currentPage() - 1)*$faqs->perPage() }}</td>
                            <td>{{ $faq->question }}</td>
                            <td>{{ $faq->answer }}</td>
                            <td class="text-right">
                                <a class="btn btn-soft-primary btn-icon btn-circle btn-sm"
                                href="{{ route('faq.edit', ['id' => $faq->id, 'lang' => env('DEFAULT_LANGUAGE')]) }}"
                                title="{{ translate('Edit') }}">
                                <i class="las la-edit"></i>
                            </a>
                            <a href="#" class="btn btn-soft-danger btn-icon btn-circle btn-sm confirm-delete"
                            data-href="{{ route('faq.destroy', $faq->id) }}"
                            title="{{ translate('Delete') }}">
                            <i class="las la-trash"></i>
                        </a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <div class="aiz-pagination">
            {{ $faqs->appends(request()->input())->links() }}
        </div>
    </div>
</div>
</div>
</div>
@endsection
@section('modal')
@include('modals.delete_modal')
@endsection

