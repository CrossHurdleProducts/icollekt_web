@extends('backend.layouts.app')
@section('content')
<div class="aiz-titlebar text-left mt-2 mb-3">
    <h5 class="mb-0 h6">{{translate('Customer Information')}}</h5>
</div>
<div class="col-sm-12">
    <div class="card">
        <div class="card-body p-0">
            @if ($errors->any())
            <div class="alert alert-danger m-4">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            <form class="p-4" action="{{ route('customers.update', $customer->id) }}" method="POST">
                @csrf
                @method('PATCH')
                <div class="form-group row">
                    <div class="col-sm-12 col-md-6">
                        <label for="name">{{ translate('Name') }}</label>
                        <input type="text" name="name" value="{{ $customer->name}}" class="form-control" id="name" placeholder="Name" required>
                    </div>
                    <div class="col-sm-12 col-md-6 ">
                        <label for="name">{{ translate('Email') }}</label>
                        <input type="text" name="email" value="{{ $customer->email}}" class="form-control" id="email" placeholder="Email" required>
                    </div>
                    <div class="col-sm-12 col-md-12 ">
                        <label for="name">{{ translate('Address') }}</label>
                        <textarea placeholder="{{ translate('Address') }}" id="address" name="address" class="form-control"  required>{{ $customer->address }}</textarea>
                </div>
                    <div class="col-sm-12 col-md-6 ">
                        <label for="name">{{ translate('D.O.B') }}</label>
                        <input  class="aiz-date-range form-control" type="text" name="date_of_birth"  value="{{ $customer->date_of_birth}}"  placeholder="{{ translate('Date of birth') }}" data-format="Y-MM-DD" data-single ="true" data-advanced-range="false" autocomplete="off">
                    </div>
                    <div class="col-sm-12 col-md-6 ">
                        <label for="name">{{ translate('Phone') }}</label>
                        <input type="text"  value="{{ $customer->phone}}" class="form-control" id="phone" name="phone"  placeholder="" required>
                    </div>
                    <div class="col-sm-12 col-md-6 ">
                        <label for="name">{{ translate('City') }}</label>
                        <input type="text" value="{{ $customer->city}}" class="form-control" id="city" name="city" placeholder="City" required>
                    </div>
                    <div class="col-sm-12 col-md-6 ">
                        <label for="name">{{ translate('Country') }}</label>
                        <select class="form-control aiz-selectpicker" data-live-search="true" id="country" name="country"  placeholder="Country" required>
                            @foreach (\App\Country::all() as $row)
                            <option value="{{ $row->code }}" @if($row->code == $customer->country) selected @endif>
                                {{ $row->name }}
                            </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-sm-12 col-md-6 ">
                        <label for="name">{{ translate('Pincode') }}</label>
                        <input type="text" value="{{ $customer->postal_code}}" class="form-control" id="postal_code" name="postal_code"  placeholder="Pincode" value="{{ $customer->postal_code }}" >
                    </div>
                </div>
                <div class="form-group mb-0 text-right">
                    <button type="submit" class="btn btn-primary">{{translate('Save')}}</button>
                    <a href="{{ route('customers.index')  }}" class="btn btn-danger">{{translate('Cancel')}}</a>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
