<div class="card-header mb-2">
    <h3 class="h6">{{ translate('Add Your Product Base Coupon') }}</h3>
</div>
<div class="form-group row">
    <label class="col-lg-3 col-from-label" for="coupon_code">{{ translate('Coupon code') }}</label>
    <div class="col-lg-9">
        <input type="text" placeholder="{{ translate('Coupon code') }}" id="coupon_code" name="coupon_code"
            class="form-control" required>
    </div>
</div>
<div class="product-choose-list">
    <div class="product-choose" id ="only-product">
        <div class="form-group row">
            <label class="col-lg-3 col-from-label" for="name">{{ translate('Product') }}</label>
            <div class="col-lg-9">
                <select name="product_ids[]" class="form-control product_id aiz-selectpicker" data-live-search="true"
                    data-selected-text-format="count"  multiple>
                    @foreach ($products as $product)
                        <option value="{{ $product->id }}">{{ $product->getTranslation('name') }}</option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>
</div>
<br>
<div class="form-group row">
    <label class="col-sm-3 control-label" for="start_date">{{ translate('Date') }}</label>
    <div class="col-sm-9">
        <input type="text" class="form-control aiz-date-range" name="date_range" placeholder="Select Date">
    </div>
</div>
<div class="form-group row">
    <label class="col-lg-3 col-from-label">{{ translate('Discount') }}</label>
    <div class="col-lg-7">
        <input type="number" lang="en" min="0" step="0.01" placeholder="{{ translate('Discount') }}" name="discount" id="discount"
            class="form-control" required>
    </div>
    <div class="col-lg-2">
        <select class="form-control aiz-selectpicker" name="discount_type" id="discount_type">
            <option value="amount">{{ translate('Amount') }}</option>
            <option value="percent">{{ translate('Percent') }}</option>
            <option value="product_to_product">{{ translate('Product to product') }}</option>
        </select>
    </div>
</div>
<div class="form-group row product_div" style="display:none;">
    <label class="col-lg-3 col-from-label">{{ translate('Offer Product') }}</label>
    <div class="col-lg-9">
        <!-- <input type="text" placeholder="{{ translate('Product Offer') }}" name="product_offer" class="form-control"
            > -->
            <select id="product_offer" name="product_offer[]" class="form-control product_id aiz-selectpicker" data-live-search="true"
                    data-selected-text-format="count" required multiple>
                    @foreach ($products as $product)
                        <option value="{{ $product->id }}">{{ $product->getTranslation('name') }}</option>
                    @endforeach
            </select>
    </div>
</div>
<div class="form-group row product_div" style="display:none;">
    <label class="col-lg-3 col-from-label">{{ translate('Offer Amount') }}</label>
    <div class="col-lg-9">
        <input type="number" lang="en" min="0" step="0.01" placeholder="{{ translate('Offer Amount') }}"
            name="offer_amount" class="form-control" >
    </div>
</div>


<script type="text/javascript">
    $(document).ready(function() {
        $('.aiz-date-range').daterangepicker();
        AIZ.plugins.bootstrapSelect('refresh');

        $("#discount_type").change(function() {
            if ($(this).val() == "product_to_product") {
                $(".product_div").show();
                $("#product_offer").attr("required", true);
                $("#offer_amount").attr("required", true);
                $("#discount").attr("disabled", true);
                //$("#only-product").attr("disabled", true);
                //$('#only-product').prop('disabled', true);
                $("#only-product").hide();


            } else {
                $(".product_div").hide();
                $("#product_offer").attr("required", false); 
                $("#offer_amount").attr("required", false);
                $("#discount").attr("disabled", false);
                $("#discount").attr("required", true);
                $("#product_offer").val("");
                $("#offer_amount").val("");
                $("#only-product").show();

            }
        })
    });
</script>
